package com.kevin.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.kevin.domain.entity.User;
import org.apache.ibatis.annotations.Param;

import java.util.List;


/**
 * 用户表(User)表数据库访问层
 *
 * @author makejava
 * @since 2023-02-09 21:54:41
 */
public interface UserMapper extends BaseMapper<User> {
    boolean insertUserRole(@Param("userId") Long userId, @Param("roleIds") List<Long> roleIds);

    boolean deleteUserRole(Long id);

    List<Long> getRoleIdsByUserId(Long id);
}
