package com.kevin.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.kevin.constants.SystemConstants;
import com.kevin.domain.ResponseResult;
import com.kevin.domain.dto.LinkListDto;
import com.kevin.domain.entity.Link;
import com.kevin.domain.vo.LinkVo;
import com.kevin.domain.vo.PageVo;
import com.kevin.enums.AppHttpCodeEnum;
import com.kevin.handler.exception.SystemException;
import com.kevin.mapper.LinkMapper;
import com.kevin.service.LinkService;
import com.kevin.utils.BeanCopyUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;

/**
 * 友链(Link)表服务实现类
 *
 * @author makejava
 * @since 2023-02-09 17:58:57
 */

/**/
@Service("linkService")
public class LinkServiceImpl extends ServiceImpl<LinkMapper, Link> implements LinkService {

    @Autowired
    private LinkMapper linkMapper;


    @Override
    public ResponseResult getAllLink() {
            //查询所有审核通过的
            LambdaQueryWrapper<Link> queryWrapper = new LambdaQueryWrapper<>();
            queryWrapper.eq(Link::getStatus, SystemConstants.LINK_STATUS_NORMAL);
            List<Link> links = list(queryWrapper);
            //转换成vo
            List<LinkVo> linkVos = BeanCopyUtils.copyBeanList(links, LinkVo.class);
            //封装返回
            return ResponseResult.okResult(linkVos);
    }

    @Override
    public ResponseResult addLink(Link link) {
        //名称  address 不能为空
        isBasicLink(link);
        //友链地址不能重复
        Boolean res = isExistLinkAddress(link);
        if (res){
            throw new SystemException(AppHttpCodeEnum.LINK_ADDRESS_EXIST);
        }
        this.save(link);
        return ResponseResult.okResult();
    }

    @Override
    public ResponseResult getLinkById(Long id) {
        if (id == null){
            throw new SystemException(AppHttpCodeEnum.NO_LINK_ID);
        }
        Link link = this.getById(id);
        return ResponseResult.okResult(link);
    }

    @Override
    public ResponseResult updateLink(Link link) {
        isBasicLink(link);
        Boolean res = isExistOtherLinkAddress(link);
        if (res){
            throw new SystemException(AppHttpCodeEnum.LINK_ADDRESS_EXIST);
        }
        this.updateById(link);
        return ResponseResult.okResult();
    }

    @Override
    public ResponseResult deleteLink(Long id) {
        if (id == null){
            throw new SystemException(AppHttpCodeEnum.NO_LINK_ID);
        }
        this.removeById(id);
        return ResponseResult.okResult();
    }

    @Override
    public ResponseResult list(Integer pageNum, Integer pageSize, LinkListDto linkListDto) {
        String name = linkListDto.getName();
        Integer status = linkListDto.getStatus();
        LambdaQueryWrapper<Link> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.likeRight(StringUtils.hasText(name), Link::getName,name);
        queryWrapper.eq(status != null, Link::getStatus,status);

        Page<Link> linkPage = new Page<>(pageNum, pageSize);
        this.page(linkPage,queryWrapper);
//        List<LinkVo> linkVos = BeanCopyUtils.copyBeanList(linkPage.getRecords(), LinkVo.class);
        PageVo pageVo = new PageVo(linkPage.getRecords(), linkPage.getTotal());
        return ResponseResult.okResult(pageVo);
    }

    @Override
    public ResponseResult changeLinkStatus(Link link) {
        Long id = link.getId();
        String status = link.getStatus();
        LambdaUpdateWrapper<Link> updateWrapper = new LambdaUpdateWrapper<>();
        updateWrapper.eq(id != null, Link::getId,id);
        updateWrapper.set(StringUtils.hasText(status), Link::getStatus,status);
        this.update(updateWrapper);
        return ResponseResult.okResult();
    }

    private void isBasicLink(Link link) {
        String name = link.getName();
        String address = link.getAddress();
        if (!StringUtils.hasText(name)){
            throw new SystemException(AppHttpCodeEnum.NEED_LINK_NAME);
        }
        if (!StringUtils.hasText(address)){
            throw new SystemException(AppHttpCodeEnum.NEED_LINK_ADDRESS);
        }
    }

    private Boolean isExistOtherLinkAddress(Link link) {
        String address = link.getAddress();
        LambdaQueryWrapper<Link> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(StringUtils.hasText(address),Link::getAddress,address);
        Link link1 = this.getOne(queryWrapper);
        if (link1 == null){
            return false;
        }
        return link.getId() != link1.getId();
    }

    /**
     * 友链地址不能重复
     * @param link
     * @return
     */
    private Boolean isExistLinkAddress(Link link) {
        String address = link.getAddress();
        LambdaQueryWrapper<Link> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(StringUtils.hasText(address),Link::getAddress,address);
        int count = count(queryWrapper);
        return count > 0;
    }
}

