package com.kevin.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.kevin.constants.SystemConstants;
import com.kevin.domain.ResponseResult;
import com.kevin.domain.dto.PageDto;
import com.kevin.domain.dto.QueryCategoryDto;
import com.kevin.domain.dto.QueryCommentListDto;
import com.kevin.domain.entity.Article;
import com.kevin.domain.entity.Category;
import com.kevin.domain.entity.Comment;
import com.kevin.domain.vo.CategoryListVo;
import com.kevin.domain.vo.CommentListVo;
import com.kevin.domain.vo.CommentVo;
import com.kevin.domain.vo.PageVo;
import com.kevin.enums.AppHttpCodeEnum;
import com.kevin.handler.exception.SystemException;
import com.kevin.mapper.CommentMapper;
import com.kevin.service.CommentService;
import com.kevin.service.UserService;
import com.kevin.utils.BeanCopyUtils;
import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.Collections;
import java.util.List;

@Service("commentService")
public class CommentServiceImpl extends ServiceImpl<CommentMapper, Comment> implements CommentService {

    @Autowired
    private UserService userService;

    @Autowired
    private CommentService commentService;

    @Override
    public ResponseResult commentList(String commentType, Long articleId, Integer pageNum, Integer pageSize) {
        //查询对应文章的根评论
        LambdaQueryWrapper<Comment> queryWrapper = new LambdaQueryWrapper<>();

        //对articleId进行判断
        queryWrapper.eq(SystemConstants.ARTICLE_COMMENT.equals(commentType),Comment::getArticleId,articleId);

        //根评论 rootId为-1
        queryWrapper.eq(Comment::getRootId,-1);

        queryWrapper.eq(Comment::getType,commentType);

        //分页查询
        Page<Comment> page = new Page(pageNum,pageSize);
        page(page,queryWrapper);

        List<CommentVo> commentVoList = toCommentVoList(page.getRecords());

        //查询所有根评论对应的子评论集合，并且赋值给对应的属性
        for (CommentVo commentVo : commentVoList) {
            //查询对应的子评论
            List<CommentVo> children = getChildren(commentVo.getId());
            //赋值
            commentVo.setChildren(children);
        }

        return ResponseResult.okResult(new PageVo<>(commentVoList,page.getTotal()));
    }


    @Override
    public ResponseResult addComment(Comment comment) {
        if(!StringUtils.hasText(comment.getContent())){
            throw new SystemException(AppHttpCodeEnum.CONTENT_NOT_NULL);
        }
        save(comment);
        return ResponseResult.okResult();
    }

    /**
     * 根据根评论的id查询所对应的子评论的集合
     * @param id 根评论的id
     * @return
     */
    private List<CommentVo> getChildren(Long id) {

        LambdaQueryWrapper<Comment> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(Comment::getRootId,id);
        queryWrapper.orderByAsc(Comment::getCreateTime);
        List<Comment> comments = list(queryWrapper);

        List<CommentVo> commentVos = toCommentVoList(comments);
        return commentVos;
    }

    private List<CommentVo> toCommentVoList(List<Comment> list){
        List<CommentVo> commentVos = BeanCopyUtils.copyBeanList(list, CommentVo.class);
        //遍历vo集合
        for (CommentVo commentVo : commentVos) {
            //通过creatyBy查询用户的昵称并赋值

            String nickName = userService.getById(commentVo.getCreateBy()).getNickName();
            commentVo.setUsername(nickName);

            //通过toCommentUserId查询用户的昵称并赋值
            //如果toCommentUserId不为-1才进行查询
            if(commentVo.getToCommentUserId()!=-1){
                String toCommentUserName = userService.getById(commentVo.getToCommentUserId()).getNickName();
                commentVo.setToCommentUserName(toCommentUserName);
            }
        }
        return commentVos;
    }

    @Override
    public ResponseResult<PageVo<CommentListVo>> listCommentByPage(PageDto pageDto, QueryCommentListDto queryCommentListDto) {
        LambdaQueryWrapper<Comment> wrapper = new LambdaQueryWrapper<>();
        wrapper.like(Strings.isNotEmpty(queryCommentListDto.getType()),Comment::getType, queryCommentListDto.getType())
                .eq(queryCommentListDto.getContent()!=null,Comment::getContent, queryCommentListDto.getContent())
                .orderByDesc(Comment::getCreateTime);

        Page<Comment> categoryPage = page(new Page<>(pageDto.getPageNum(), pageDto.getPageSize()), wrapper);

        List<Comment> categoryList = categoryPage.getRecords();

        List<CommentListVo> categoryListVos = BeanCopyUtils.copyBeanList(categoryList, CommentListVo.class);
        PageVo<CommentListVo> pageVo = new PageVo<>(categoryListVos, categoryPage.getTotal());
        return ResponseResult.success(pageVo);
    }

    @Override
    public ResponseResult<Object> deleteWithValidById(List<Long> ids, boolean b) {
        removeByIds(ids);
        return ResponseResult.success();
    }



}
