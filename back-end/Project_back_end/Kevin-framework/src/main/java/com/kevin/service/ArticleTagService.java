package com.kevin.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.kevin.domain.entity.ArticleTag;

/**
 * 文章标签关联表(ArticleTag)表服务接口
 *
 * @author Yozuru
 * @since 2022-12-02 20:01:14
 */

public interface ArticleTagService extends IService<ArticleTag> {

}

