package com.kevin.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.kevin.constants.SystemConstants;
import com.kevin.domain.ResponseResult;
import com.kevin.domain.dto.CategoryDto;
import com.kevin.domain.dto.PageDto;
import com.kevin.domain.dto.QueryCategoryDto;
import com.kevin.domain.entity.Article;
import com.kevin.domain.entity.Category;
import com.kevin.domain.vo.CategoryListVo;
import com.kevin.domain.vo.CategoryVo;
import com.kevin.domain.vo.PageVo;
import com.kevin.mapper.CategoryMapper;
import com.kevin.service.ArticleService;
import com.kevin.service.CategoryService;
import com.kevin.utils.BeanCopyUtils;
import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * 分类表(Category)表服务实现类
 *
 * @author makejava
 * @since 2023-02-09 16:07:50
 */
@Service("categoryService")
public class CategoryServiceImpl extends ServiceImpl<CategoryMapper, Category> implements CategoryService {

    @Autowired
    private ArticleService articleService;

    @Override
    public ResponseResult getCategoryList() {
            //查询文章表  状态为已发布的文章
            LambdaQueryWrapper<Article> articleWrapper = new LambdaQueryWrapper<>();
            articleWrapper.eq(Article::getStatus, SystemConstants.ARTICLE_STATUS_NORMAL);
            List<Article> articleList = articleService.list(articleWrapper);
            //获取文章的分类id，并且去重
            Set<Long> categoryIds = articleList.stream()
                    .map(article -> article.getCategoryId())
                    .collect(Collectors.toSet());

            //查询分类表
            List<Category> categories = listByIds(categoryIds);
            categories = categories.stream().
                    filter(category -> SystemConstants.STATUS_NORMAL.equals(category.getStatus()))
                    .collect(Collectors.toList());
            //封装vo
            List<CategoryVo> categoryVos = BeanCopyUtils.copyBeanList(categories, CategoryVo.class);

            return ResponseResult.okResult(categoryVos);
    }

    @Override
    public ResponseResult<List<CategoryVo>> getAllCategory() {
        LambdaQueryWrapper<Category> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(Category::getStatus, SystemConstants.STATUS_NORMAL)
                .select(Category::getId, Category::getName);
        List<Category> categoryList = list(wrapper);
        List<CategoryVo> categoryVos = BeanCopyUtils.copyBeanList(categoryList, CategoryVo.class);
        return ResponseResult.success(categoryVos);
    }

    @Override
    public ResponseResult<PageVo<CategoryListVo>> listCategoryByPage(PageDto pageDto, QueryCategoryDto queryCategoryDto) {
        LambdaQueryWrapper<Category> wrapper = new LambdaQueryWrapper<>();
        wrapper.like(Strings.isNotEmpty(queryCategoryDto.getName()),Category::getName, queryCategoryDto.getName())
                .eq(queryCategoryDto.getStatus()!=null,Category::getStatus, queryCategoryDto.getStatus())
                .orderByDesc(Category::getCreateTime);

        Page<Category> categoryPage = page(new Page<>(pageDto.getPageNum(), pageDto.getPageSize()), wrapper);

        List<Category> categoryList = categoryPage.getRecords();

        List<CategoryListVo> categoryListVos = BeanCopyUtils.copyBeanList(categoryList, CategoryListVo.class);
        PageVo<CategoryListVo> pageVo = new PageVo<>(categoryListVos, categoryPage.getTotal());
        return ResponseResult.success(pageVo);
    }

    @Override
    public ResponseResult<Object> addCategory(CategoryDto categoryDto) {
        Category category = BeanCopyUtils.copyBean(categoryDto, Category.class);
        save(category);
        return ResponseResult.success();
    }

    @Override
    public ResponseResult<CategoryDto> getCategoryById(Integer id) {
        Category category = getById(id);
        CategoryDto categoryDto = BeanCopyUtils.copyBean(category, CategoryDto.class);
        return ResponseResult.success(categoryDto);
    }

    @Override
    public ResponseResult<Object> updateCategory(CategoryDto categoryDto) {
        Category category = BeanCopyUtils.copyBean(categoryDto, Category.class);
        updateById(category);
        return ResponseResult.success();
    }

    @Override
    public ResponseResult<Object> deleteCategory(Long id) {
        removeById(id);
        return ResponseResult.success();
    }


}
