package com.kevin.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.kevin.domain.entity.Category;


/**
 * 分类表(Category)表数据库访问层
 *
 * @author makejava
 * @since 2023-02-09 16:09:14
 */
public interface CategoryMapper extends BaseMapper<Category> {

}
