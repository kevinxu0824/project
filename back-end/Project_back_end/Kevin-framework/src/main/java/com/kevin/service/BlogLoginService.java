package com.kevin.service;

import com.kevin.domain.ResponseResult;
import com.kevin.domain.entity.User;

public interface BlogLoginService {
    ResponseResult login(User user);

    ResponseResult logout();
}
