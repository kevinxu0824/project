package com.kevin.service;

import com.kevin.domain.ResponseResult;
import com.kevin.domain.entity.User;

public interface LoginService {
    ResponseResult login(User user);

    ResponseResult logout();
}
