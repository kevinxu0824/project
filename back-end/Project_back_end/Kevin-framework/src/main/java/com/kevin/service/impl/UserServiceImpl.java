package com.kevin.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.kevin.domain.ResponseResult;
import com.kevin.domain.dto.PageDto;
import com.kevin.domain.dto.QueryUserDto;
import com.kevin.domain.dto.UserDto;
import com.kevin.domain.entity.User;
import com.kevin.domain.vo.*;
import com.kevin.enums.AppHttpCodeEnum;
import com.kevin.handler.exception.BusinessException;
import com.kevin.handler.exception.SystemException;
import com.kevin.mapper.UserMapper;
import com.kevin.service.UserService;
import com.kevin.utils.BeanCopyUtils;
import com.kevin.utils.SecurityUtils;
import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.util.List;

/**
 * 用户表(User)表服务实现类
 *
 * @author makejava
 * @since 2023-03-05 21:29:03
 */
@Service("userService")
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements UserService {
    @Override
    public ResponseResult userInfo() {
        //获取当前用户id
        Long userId = SecurityUtils.getUserId();
        //根据用户id查询用户信息
        User user = getById(userId);
        //封装成UserInfoVo
        UserInfoVo vo = BeanCopyUtils.copyBean(user,UserInfoVo.class);
        return ResponseResult.okResult(vo);
    }

    @Override
    public ResponseResult updateUserInfo(User user) {
        updateById(user);
        return ResponseResult.okResult();
    }

    @Autowired
    private PasswordEncoder passwordEncoder;
    @Override
    public ResponseResult register(User user) {
        if(!StringUtils.hasText(user.getUserName())){
            throw new SystemException(AppHttpCodeEnum.USERNAME_NOT_NULL);
        }
        if(!StringUtils.hasText(user.getPassword())){
            throw new SystemException(AppHttpCodeEnum.PASSWORD_NOT_NULL);
        }
        if(!StringUtils.hasText(user.getEmail())){
            throw new SystemException(AppHttpCodeEnum.EMAIL_NOT_NULL);
        }
        if(!StringUtils.hasText(user.getNickName())){
            throw new SystemException(AppHttpCodeEnum.NICKNAME_NOT_NULL);
        }
        //对数据进行是否存在的判断
        if(userNameExist(user.getUserName())){
            throw new SystemException(AppHttpCodeEnum.USERNAME_EXIST);
        }
        if(nickNameExist(user.getNickName())){
            throw new SystemException(AppHttpCodeEnum.NICKNAME_EXIST);
        }
        //...
        //对密码进行加密
        String encodePassword = passwordEncoder.encode(user.getPassword());
        user.setPassword(encodePassword);
        //存入数据库
        save(user);
        return ResponseResult.okResult();
    }

    @Override
    public ResponseResult<PageVo<UserVo>> getUserListByPage(QueryUserDto queryUserDto, PageDto pageDto) {
        LambdaQueryWrapper<User> wrapper = new LambdaQueryWrapper<>();
        wrapper.like(Strings.isNotEmpty(queryUserDto.getUserName()),User::getUserName,queryUserDto.getUserName())
                .eq(Strings.isNotEmpty(queryUserDto.getPhonenumber()),User::getPhonenumber,queryUserDto.getPhonenumber())
                .eq(Strings.isNotEmpty(queryUserDto.getStatus()),User::getStatus,queryUserDto.getStatus())
                .orderByAsc(User::getCreateTime);
        Page<User> pageObj = new Page<>(pageDto.getPageNum(),pageDto.getPageSize());
        pageObj= page(pageObj, wrapper);
        List<User> users = pageObj.getRecords();
        List<UserVo> userVos = BeanCopyUtils.copyBeanList(users, UserVo.class);
        PageVo<UserVo> pageVo = new PageVo<>(userVos,pageObj.getTotal());
        return ResponseResult.success(pageVo);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ResponseResult<Object> addUser(UserDto userDto) {
        if (userNameExist(userDto.getUserName()))
            throw new BusinessException(AppHttpCodeEnum.USERNAME_EXIST);
        if (emailExist(userDto.getEmail()))
            throw new BusinessException(AppHttpCodeEnum.EMAIL_EXIST);
        if (phoneExist(userDto.getPhonenumber()))
            throw new BusinessException(AppHttpCodeEnum.PHONENUMBER_EXIST);
        userDto.setPassword(passwordEncoder.encode(userDto.getPassword()));
        User user=BeanCopyUtils.copyBean(userDto,User.class);
        save(user);
        baseMapper.insertUserRole(user.getId(),userDto.getRoleIds());
        return ResponseResult.success();
    }

    @Override
    public ResponseResult<Object> deleteUserById(Long id) {
        removeById(id);
        baseMapper.deleteUserRole(id);
        return ResponseResult.success();
    }

    @Override
    public UpdateUserVo getUserInfoById(Long id) {
        User user = getById(id);
        UpdateUserInfoVo updateUserInfoVo = BeanCopyUtils.copyBean(user, UpdateUserInfoVo.class);
        UpdateUserVo updateUserVo = new UpdateUserVo();
        updateUserVo.setUser(updateUserInfoVo);
        List<Long> roleIds = baseMapper.getRoleIdsByUserId(id);
        updateUserVo.setRoleIds(roleIds);
        return updateUserVo;
    }

    @Override
    public ResponseResult<Object> updateUser(UserDto userDto) {
        User nowUser = getById(userDto.getId());
        if (!nowUser.getUserName().equals(userDto.getUserName())&&userNameExist(userDto.getUserName()))
            throw new BusinessException(AppHttpCodeEnum.USERNAME_EXIST);
        if (!nowUser.getEmail().equals(userDto.getEmail())&&emailExist(userDto.getEmail()))
            throw new BusinessException(AppHttpCodeEnum.EMAIL_EXIST);
        User user=BeanCopyUtils.copyBean(userDto,User.class);
        updateById(user);
        baseMapper.deleteUserRole(user.getId());
        baseMapper.insertUserRole(user.getId(),userDto.getRoleIds());
        return ResponseResult.success();
    }

    private boolean userNameExist(String userName){
        LambdaQueryWrapper<User> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(User::getUserName,userName);
        return count(queryWrapper) > 0;
    }

    private boolean nickNameExist(String nickName){
        LambdaQueryWrapper<User> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(User::getNickName,nickName);
        return count(queryWrapper) > 0;
    }

    private boolean emailExist(String email){
        LambdaQueryWrapper<User> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(User::getEmail,email);
        return count(queryWrapper) > 0;
    }
    private boolean phoneExist(String phone){
        LambdaQueryWrapper<User> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(User::getPhonenumber,phone);
        return count(queryWrapper) > 0;
    }
}
