package com.kevin.service.impl;

import com.alicp.jetcache.Cache;
import com.alicp.jetcache.anno.CacheType;
import com.alicp.jetcache.anno.CreateCache;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.kevin.constants.RedisConstant;
import com.kevin.constants.SystemConstants;
import com.kevin.domain.ResponseResult;
import com.kevin.domain.dto.ArticleDto;
import com.kevin.domain.dto.PageDto;
import com.kevin.domain.dto.QueryArticleListDto;
import com.kevin.domain.entity.Article;
import com.kevin.domain.entity.ArticleTag;
import com.kevin.domain.entity.Category;
import com.kevin.domain.vo.ArticleDetailVo;
import com.kevin.domain.vo.ArticleListVo;
import com.kevin.domain.vo.HotArticleVo;
import com.kevin.domain.vo.PageVo;
import com.kevin.mapper.ArticleMapper;
import com.kevin.service.ArticleService;
import com.kevin.service.ArticleTagService;
import com.kevin.service.CategoryService;
import com.kevin.utils.BeanCopyUtils;
import com.kevin.utils.RedisCache;
import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
public class ArticleServiceImpl extends ServiceImpl<ArticleMapper,Article> implements ArticleService {

    @Autowired
    private CategoryService categoryService;

    @Autowired
    private RedisCache redisCache;

    @Autowired
    private ArticleTagService articleTagService;

    @CreateCache(area = "viewCount",name = RedisConstant.VIEW_COUNT_KEY_PREFIX,cacheType = CacheType.REMOTE)
    private Cache<Long, Long> viewCountCache;

//    @Autowired
//    private ArticleTagService articleTagService;

    @Override
    public ResponseResult hotArticleList() {
        //查询热门文章
        LambdaQueryWrapper<Article> queryWrapper = new LambdaQueryWrapper<>();

        //必须是正式文章
        queryWrapper.eq(Article::getStatus, SystemConstants.ARTICLE_STATUS_NORMAL);

        //按照浏览量进行排序
        queryWrapper.orderByDesc(Article::getViewCount);
        //最多只能查出10条消息
        Page<Article> page = new Page(1,10);
        page(page,queryWrapper);

        List<Article> articles = page.getRecords();
        //bean拷贝

        List<HotArticleVo> vs = BeanCopyUtils.copyBeanList(articles, HotArticleVo.class);

        return ResponseResult.okResult(vs);
    }

    @Override
    public ResponseResult articleList(Integer pageNum, Integer pageSize, Long categoryId) {
        //查询条件
        LambdaQueryWrapper<Article> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        // 如果 有categoryId 就要 查询时要和传入的相同
        lambdaQueryWrapper.eq(Objects.nonNull(categoryId)&&categoryId>0 ,Article::getCategoryId,categoryId);

        // 状态是正式发布的
        lambdaQueryWrapper.eq(Article::getStatus,SystemConstants.ARTICLE_STATUS_NORMAL);
        // 对isTop进行降序
        lambdaQueryWrapper.orderByDesc(Article::getIsTop);

        //分页查询
        Page<Article> page = new Page<>(pageNum,pageSize);
        page(page,lambdaQueryWrapper);

        //查询categoryName
        List<Article> articles = page.getRecords();
        articles.stream()
                .map(article -> article.setCategoryName(categoryService.getById(article.getCategoryId()).getName()))
                .collect(Collectors.toList());

        //articleId去查询articleName进行设置
        for (Article article : articles) {
            Category category = categoryService.getById(article.getCategoryId());
            article.setCategoryName(category.getName());
        }

        //封装查询结果
        List<ArticleListVo> articleListVos = BeanCopyUtils.copyBeanList(page.getRecords(), ArticleListVo.class);

        PageVo<ArticleListVo> pageVo = new PageVo<>(articleListVos,page.getTotal());
        return ResponseResult.okResult(pageVo);
    }

    @Override
    public ResponseResult getArticleDetail(Long id) {
        //根据id查询文章
        Article article = getById(id);

        //从redis中获取viewCount
        Integer viewCount = redisCache.getCacheMapValue("article:viewCount", id.toString());
        article.setViewCount(viewCount.longValue());

        //转换成VO
        ArticleDetailVo articleDetailVo = BeanCopyUtils.copyBean(article, ArticleDetailVo.class);
        //根据分类id查询分类名
        Long categoryId = articleDetailVo.getCategoryId();
        Category category = categoryService.getById(categoryId);
        if(category!=null){
            articleDetailVo.setCategoryName(category.getName());
        }
        //封装响应返回
        return ResponseResult.okResult(articleDetailVo);
    }


    @Override
    public ResponseResult updateViewCount(Long id) {
        //更新redis中对应 id的浏览量
        redisCache.incrementCacheMapValue("article:viewCount",id.toString(),1);
        return ResponseResult.okResult();
    }


    @Override
    @Transactional
    public ResponseResult addArticle(ArticleDto articleDto) {
        //添加 博客
        Article article = BeanCopyUtils.copyBean(articleDto, Article.class);
        save(article);
        List<Long> tagIds = articleDto.getTags();
        List<ArticleTag> articleTagList = tagIds.stream()
                .map(tagId -> new ArticleTag(article.getId(), tagId))
                .collect(Collectors.toList());
        articleTagService.saveBatch(articleTagList);
        //添加文章浏览量的缓存
//        viewCountCache.put(article.getId(),0L);

        return ResponseResult.success();

    }

    @Override
    public ResponseResult<PageVo<ArticleListVo>> getAdminArticleList(QueryArticleListDto queryArticleListDto, PageDto pageDto) {
        //设置条件查询
        LambdaQueryWrapper<Article> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.like(Strings.isNotBlank(queryArticleListDto.getTitle()),Article::getTitle, queryArticleListDto.getTitle())
                .like(Strings.isNotBlank(queryArticleListDto.getSummary()),Article::getSummary, queryArticleListDto.getSummary())
                .orderByDesc(Article::getIsTop)
                .orderByDesc(Article::getCreateTime);
        //设置分页查询
        Page<Article> pageData = new Page<>(pageDto.getPageNum(), pageDto.getPageSize());
        //查询文章列表
        pageData = page(pageData, queryWrapper);
        //封装进VO
        List<Article> records = pageData.getRecords();
        List<ArticleListVo> adminArticleVoList = BeanCopyUtils.copyBeanList(records, ArticleListVo.class);
        PageVo<ArticleListVo> pageVo = new PageVo<>(adminArticleVoList, pageData.getTotal());
        return ResponseResult.success(pageVo);
    }

    @Override
    public ResponseResult<ArticleDto> getAdminArticleDetail(Long id) {
        LambdaQueryWrapper<Article> articleWrapper = new LambdaQueryWrapper<>();
        articleWrapper.eq(Article::getId, id);
        Article article = getOne(articleWrapper);
        ArticleDto articleDto = BeanCopyUtils.copyBean(article, ArticleDto.class);

        //查询文章的标签
        LambdaQueryWrapper<ArticleTag> articleTagWrapper = new LambdaQueryWrapper<>();
        articleTagWrapper.select(ArticleTag::getTagId)
                .eq(ArticleTag::getArticleId, id);
        List<Long> tagList = articleTagService.list(articleTagWrapper)
                .stream()
                .map(ArticleTag::getTagId)
                .collect(Collectors.toList());
        articleDto.setTags(tagList);
        return ResponseResult.success(articleDto);
    }

    @Override
    public ResponseResult<Object> updateArticle(ArticleDto articleDto) {
        Article article = BeanCopyUtils.copyBean(articleDto, Article.class);
        //更新文章
        updateById(article);
        //更新文章和标签的关系
        LambdaQueryWrapper<ArticleTag> articleTagWrapper = new LambdaQueryWrapper<>();
        articleTagWrapper.eq(ArticleTag::getArticleId, article.getId());
        //先删除原来的关系
        articleTagService.remove(articleTagWrapper);
        List<Long> tagIds = articleDto.getTags();
        List<ArticleTag> articleTagList = tagIds.stream()
                .map(tagId -> new ArticleTag(article.getId(), tagId))
                .collect(Collectors.toList());
        //再添加新的关系
        articleTagService.saveBatch(articleTagList);
        return ResponseResult.success();
    }

    @Override
    public boolean updateViewCountById(Long id, Long viewCount) {
        baseMapper.updateViewCountById(id,viewCount);
        return true;
    }

    @Override
    public ResponseResult<Object> deleteArticle(Long id) {
        //删除文章
        removeById(id);
        //删除文章和标签的关系
        LambdaQueryWrapper<ArticleTag> articleTagWrapper = new LambdaQueryWrapper<>();
        articleTagWrapper.eq(ArticleTag::getArticleId, id);
        articleTagService.remove(articleTagWrapper);
        //删除文章浏览量的缓存
//        viewCountCache.remove(id);
        return ResponseResult.success();
    }

}
