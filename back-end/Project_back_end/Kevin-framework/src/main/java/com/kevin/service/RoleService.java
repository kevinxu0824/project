package com.kevin.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.kevin.domain.ResponseResult;
import com.kevin.domain.dto.PageDto;
import com.kevin.domain.dto.RoleDto;
import com.kevin.domain.entity.Role;
import com.kevin.domain.vo.PageVo;
import com.kevin.domain.vo.RoleVo;
import com.kevin.domain.vo.SimpleRoleVo;

import java.util.List;


/**
 * 角色信息表(Role)表服务接口
 *
 * @author makejava
 * @since 2023-03-07 17:03:11
 */
public interface RoleService extends IService<Role> {

    List<String> selectRoleKeyByUserId(Long id);

    ResponseResult<PageVo<RoleVo>> getRoleListByPage(PageDto pageDto, String roleName, String status);

    ResponseResult<Object> updateStatus(Long roleId, String status);

    ResponseResult<Object> addRole(RoleDto roleDto);

    ResponseResult<RoleVo> getRoleById(Long id);

    ResponseResult<Object> updateRole(RoleDto roleDto);

    ResponseResult<Object> deleteRoleById(Long id);

    ResponseResult<List<SimpleRoleVo>> getAllRole();
}
