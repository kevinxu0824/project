package com.kevin.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.kevin.domain.ResponseResult;
import com.kevin.domain.dto.CategoryDto;
import com.kevin.domain.dto.PageDto;
import com.kevin.domain.dto.QueryCategoryDto;
import com.kevin.domain.entity.Category;
import com.kevin.domain.vo.CategoryListVo;
import com.kevin.domain.vo.CategoryVo;
import com.kevin.domain.vo.PageVo;

import java.util.List;


/**
 * 分类表(Category)表服务接口
 *
 * @author makejava
 * @since 2023-02-09 16:07:50
 */
public interface CategoryService extends IService<Category> {
    ResponseResult getCategoryList();

    ResponseResult<List<CategoryVo>> getAllCategory();

    ResponseResult<PageVo<CategoryListVo>> listCategoryByPage(PageDto pageDto, QueryCategoryDto queryCategoryDto);

    ResponseResult<Object> addCategory(CategoryDto categoryDto);

    ResponseResult<CategoryDto> getCategoryById(Integer id);

    ResponseResult<Object> updateCategory(CategoryDto categoryDto);

    ResponseResult<Object> deleteCategory(Long id);
}
