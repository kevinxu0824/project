package com.kevin.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.kevin.domain.ResponseResult;
import com.kevin.domain.dto.PageDto;
import com.kevin.domain.dto.QueryCategoryDto;
import com.kevin.domain.dto.QueryCommentListDto;
import com.kevin.domain.entity.Comment;
import com.kevin.domain.vo.ArticleListVo;
import com.kevin.domain.vo.CategoryListVo;
import com.kevin.domain.vo.CommentListVo;
import com.kevin.domain.vo.PageVo;

import java.util.List;


/**
 * 评论表(Comment)表服务接口
 *
 * @author makejava
 * @since 2023-03-05 21:19:04
 */
public interface CommentService extends IService<Comment> {

    ResponseResult commentList(String linkComment, Long articleId, Integer pageNum, Integer pageSize);

    ResponseResult addComment(Comment comment);

    ResponseResult<PageVo<CommentListVo>> listCommentByPage(PageDto pageDto, QueryCommentListDto queryCommentListDto);

    ResponseResult<Object> deleteWithValidById(List<Long> ids, boolean b);
}
