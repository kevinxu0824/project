package com.kevin.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.kevin.domain.ResponseResult;
import com.kevin.domain.dto.TagDto;
import com.kevin.domain.dto.TagListDto;
import com.kevin.domain.entity.Tag;
import com.kevin.domain.vo.PageVo;
import com.kevin.domain.vo.TagVo;

import java.util.List;


/**
 * 标签(Tag)表服务接口
 *
 * @author makejava
 * @since 2023-03-07 10:56:05
 */
public interface TagService extends IService<Tag> {
    ResponseResult<PageVo> getTagPageList(Integer pageNum, Integer pageSize, TagListDto tagListDto);

    ResponseResult<TagVo> getVoById(Long id);

    ResponseResult<List<TagVo>> getAllTag();

    ResponseResult<Object> addTag(TagDto tagDto);

    ResponseResult<Object> deleteTag(Long id);

    ResponseResult<Object> updateTag(TagDto tagDto);
}
