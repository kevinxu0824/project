package com.kevin.handler.exception;

import com.kevin.enums.AppHttpCodeEnum;
import lombok.Getter;
import lombok.ToString;

/**
 * @author Yozuru
 */
@Getter
@ToString
public class BusinessException extends RuntimeException{
    private final Integer code;
    private final String message;

    public BusinessException(AppHttpCodeEnum httpCodeEnum){
        super(httpCodeEnum.getMsg());
        this.code=httpCodeEnum.getCode();
        this.message= httpCodeEnum.getMsg();
    }
}
