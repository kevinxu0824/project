package com.kevin.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.kevin.domain.entity.Link;


/**
 * 友链(Link)表数据库访问层
 *
 * @author makejava
 * @since 2023-02-09 17:58:54
 */
public interface LinkMapper extends BaseMapper<Link> {

}
