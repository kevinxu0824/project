package com.kevin.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.kevin.domain.ResponseResult;
import com.kevin.domain.entity.Menu;
import com.kevin.domain.vo.MenuVo;
import com.kevin.domain.vo.SimpleMenuVo;

import java.util.List;


/**
 * 菜单权限表(Menu)表服务接口
 *
 * @author makejava
 * @since 2023-03-07 16:59:41
 */
public interface MenuService extends IService<Menu> {
    List<String> selectPermsByUserId(Long id);

    List<Menu> selectRouterMenuTreeByUserId(Long userId);

    ResponseResult<List<MenuVo>> selectMenuList(String name, String status);

    ResponseResult<Object> addMenu(Menu menu);

    ResponseResult<Object> updateMenu(Menu menu);

    ResponseResult<MenuVo> getMenuById(Long id);

    ResponseResult<Object> deleteMenu(Long id);

    ResponseResult<List<SimpleMenuVo>> selectTreeAll();

    List<Long> selectMenuIdsByRoleId(Long id);
}
