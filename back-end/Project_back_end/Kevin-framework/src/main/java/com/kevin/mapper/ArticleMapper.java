package com.kevin.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.kevin.domain.entity.Article;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Update;

public interface ArticleMapper extends BaseMapper<Article> {
    @Update("update yozuru_article set view_count = #{view_count} + 1 where id = #{id}")
    void updateViewCountById(@Param("id") Long id, @Param("view_count")  Long viewCount);
}
