package com.kevin.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.kevin.domain.entity.ArticleTag;

public interface ArticleTagMapper  extends BaseMapper<ArticleTag> {
}
