package com.kevin.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class LinkListDto {
    //分类名
    private String name;
    //状态
    private Integer status;
}