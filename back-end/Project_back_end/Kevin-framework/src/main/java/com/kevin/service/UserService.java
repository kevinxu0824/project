package com.kevin.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.kevin.domain.ResponseResult;
import com.kevin.domain.dto.PageDto;
import com.kevin.domain.dto.QueryUserDto;
import com.kevin.domain.dto.UserDto;
import com.kevin.domain.entity.User;
import com.kevin.domain.vo.PageVo;
import com.kevin.domain.vo.UpdateUserVo;
import com.kevin.domain.vo.UserVo;


/**
 * 用户表(User)表服务接口
 *
 * @author makejava
 * @since 2023-03-05 21:29:02
 */
public interface UserService extends IService<User> {
    ResponseResult userInfo();

    ResponseResult updateUserInfo(User user);

    ResponseResult register(User user);

    ResponseResult<PageVo<UserVo>> getUserListByPage(QueryUserDto queryUserDto, PageDto pageDto);

    ResponseResult<Object> addUser(UserDto userDto);

    ResponseResult<Object> deleteUserById(Long id);

    UpdateUserVo getUserInfoById(Long id);

    ResponseResult<Object> updateUser(UserDto userDto);
}
