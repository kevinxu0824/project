package com.kevin.domain.dto;


import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class QueryCommentListDto  {
//    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    private String type;
    private String content;
    private Date createTime;
}
