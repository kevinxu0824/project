package com.kevin.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.kevin.constants.SystemConstants;
import com.kevin.domain.ResponseResult;
import com.kevin.domain.dto.PageDto;
import com.kevin.domain.dto.RoleDto;
import com.kevin.domain.entity.Role;
import com.kevin.domain.vo.PageVo;
import com.kevin.domain.vo.RoleVo;
import com.kevin.domain.vo.SimpleRoleVo;
import com.kevin.enums.AppHttpCodeEnum;
import com.kevin.mapper.RoleMapper;
import com.kevin.service.RoleService;
import com.kevin.utils.BeanCopyUtils;
import org.apache.logging.log4j.util.Strings;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * 角色信息表(Role)表服务实现类
 *
 * @author makejava
 * @since 2023-03-07 17:03:11
 */
@Service("roleService")
public class RoleServiceImpl extends ServiceImpl<RoleMapper, Role> implements RoleService {

    @Override
    public List<String> selectRoleKeyByUserId(Long id) {
        //判断是否是管理员 如果是返回集合中只需要有admin
        if(id == 1L){
            List<String> roleKeys = new ArrayList<>();
            roleKeys.add("admin");
            return roleKeys;
        }
        //否则查询用户所具有的角色信息
        return getBaseMapper().selectRoleKeyByUserId(id);
    }

    @Override
    public ResponseResult<PageVo<RoleVo>> getRoleListByPage(PageDto pageDto, String roleName, String status) {
        LambdaQueryWrapper<Role> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.like(Strings.isNotEmpty(roleName),Role::getRoleName, roleName)
                .eq(Strings.isNotEmpty(status),Role::getStatus, status)
                .orderByAsc(Role::getRoleSort);
        Page<Role> pageObj = new Page<Role>(pageDto.getPageNum(), pageDto.getPageSize());
        pageObj=page(pageObj,queryWrapper);
        List<Role> roleList = pageObj.getRecords();
        List<RoleVo> roleVoList = BeanCopyUtils.copyBeanList(roleList, RoleVo.class);
        PageVo<RoleVo> pageVo = new PageVo<>(roleVoList, pageObj.getTotal());
        return ResponseResult.success(pageVo);
    }

    @Override
    public ResponseResult<Object> updateStatus(Long roleId, String status) {
        Role role = getById(roleId);
        if (Objects.isNull(role)) {
            return ResponseResult.errorResult(AppHttpCodeEnum.ROLE_NOT_EXIT);
        }
        role.setStatus(status);
        updateById(role);
        return ResponseResult.success();
    }

    @Override
    public ResponseResult<Object> addRole(RoleDto roleDto) {
        Role role = BeanCopyUtils.copyBean(roleDto, Role.class);
        save(role);
        List<Long> menuIds = roleDto.getMenuIds();
        if (menuIds != null && menuIds.size() > 0) {
            baseMapper.insertRoleMenu(menuIds, role.getId());
        }
        return ResponseResult.success();
    }

    @Override
    public ResponseResult<RoleVo> getRoleById(Long id) {
        Role role = getById(id);
        if (Objects.isNull(role)) {
            return ResponseResult.errorResult(AppHttpCodeEnum.ROLE_NOT_EXIT);
        }
        RoleVo roleVo = BeanCopyUtils.copyBean(role, RoleVo.class);
        return ResponseResult.success(roleVo);
    }

    @Override
    public ResponseResult<Object> updateRole(RoleDto roleDto) {
        Role role = BeanCopyUtils.copyBean(roleDto, Role.class);
        updateById(role);
        List<Long> menuIds = roleDto.getMenuIds();
        if (menuIds != null && menuIds.size() > 0) {
            baseMapper.deleteRoleMenuByRoleId(role.getId());
            baseMapper.insertRoleMenu(menuIds,role.getId());
        }else {
            baseMapper.deleteRoleMenuByRoleId(role.getId());
        }
        return ResponseResult.success();
    }

    @Override
    public ResponseResult<Object> deleteRoleById(Long id) {
        if (id == SystemConstants.ADMIN_ID) {
            return ResponseResult.errorResult(AppHttpCodeEnum.CAN_NOT_DELETE_ADMIN);
        }
        baseMapper.deleteRoleMenuByRoleId(id);
        removeById(id);
        return ResponseResult.success();
    }

    @Override
    public ResponseResult<List<SimpleRoleVo>> getAllRole() {
        List<Role> roleList = list();
        List<SimpleRoleVo> simpleRoleVoList = BeanCopyUtils.copyBeanList(roleList, SimpleRoleVo.class);
        return ResponseResult.success(simpleRoleVoList);
    }

}
