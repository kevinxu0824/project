package com.kevin.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.kevin.domain.entity.Role;
import org.apache.ibatis.annotations.Param;

import java.util.List;


/**
 * 角色信息表(Role)表数据库访问层
 *
 * @author makejava
 * @since 2023-03-07 17:03:09
 */
public interface RoleMapper extends BaseMapper<Role> {
    List<String> selectRoleKeyByUserId(Long userId);

    boolean insertRoleMenu(@Param("menuIds") List<Long> menuIds, @Param("roleId") Long roleId);

    boolean deleteRoleMenuByRoleId(Long id);
}
