package com.kevin.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.kevin.constants.SystemConstants;
import com.kevin.domain.ResponseResult;
import com.kevin.domain.entity.Menu;
import com.kevin.domain.vo.MenuVo;
import com.kevin.domain.vo.SimpleMenuVo;
import com.kevin.enums.AppHttpCodeEnum;
import com.kevin.mapper.MenuMapper;
import com.kevin.service.MenuService;
import com.kevin.utils.BeanCopyUtils;
import com.kevin.utils.SecurityUtils;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * 菜单权限表(Menu)表服务实现类
 *
 * @author makejava
 * @since 2023-03-07 16:59:42
 */
@Service("menuService")
public class MenuServiceImpl extends ServiceImpl<MenuMapper, Menu> implements MenuService {


    @Override
    public List<String> selectPermsByUserId(Long id) {
            //如果是管理员，返回所有的权限
            if(id == 1L){
                LambdaQueryWrapper<Menu> wrapper = new LambdaQueryWrapper<>();
                wrapper.in(Menu::getMenuType, SystemConstants.MENU,SystemConstants.BUTTON);
                wrapper.eq(Menu::getStatus,SystemConstants.STATUS_NORMAL);
                List<Menu> menus = list(wrapper);
                List<String> perms = menus.stream()
                        .map(Menu::getPerms)
                        .collect(Collectors.toList());
                return perms;
            }
            //否则返回所具有的权限
            return getBaseMapper().selectPermsByUserId(id);
        }

    @Override
    public List<Menu> selectRouterMenuTreeByUserId(Long userId) {
        MenuMapper menuMapper = getBaseMapper();
        List<Menu> menus = null;
        //判断是否是管理员
        if(SecurityUtils.isAdmin()){
            //如果是 获取所有符合要求的Menu
            menus = menuMapper.selectAllRouterMenu();
        }else{
            //否则  获取当前用户所具有的Menu
            menus = menuMapper.selectRouterMenuTreeByUserId(userId);
        }

        //构建tree
        //先找出第一层的菜单  然后去找他们的子菜单设置到children属性中
        List<Menu> menuTree = builderMenuTree(menus,0L);
        return menuTree;
    }

    @Override
    public ResponseResult<List<MenuVo>> selectMenuList(String name, String status) {
        LambdaQueryWrapper<Menu> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.like(Objects.nonNull(name), Menu::getMenuName, name)
                .eq(Objects.nonNull(status), Menu::getStatus, status)
                .orderByAsc(Menu::getId, Menu::getOrderNum);
        List<Menu> list = list(queryWrapper);
        List<MenuVo> menuVos = BeanCopyUtils.copyBeanList(list, MenuVo.class);
        return ResponseResult.success(menuVos);
    }

    @Override
    public ResponseResult<Object> addMenu(Menu menu) {
        save(menu);
        return ResponseResult.success();
    }

    @Override
    public ResponseResult<Object> updateMenu(Menu menu) {
        if (menu.getParentId().equals(menu.getId())) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARENT_MENU_ERROR);
        }
        updateById(menu);
        return ResponseResult.success();
    }

    @Override
    public ResponseResult<MenuVo> getMenuById(Long id) {
        Menu menu = getById(id);
        MenuVo menuVo = BeanCopyUtils.copyBean(menu, MenuVo.class);
        return ResponseResult.success(menuVo);
    }

    @Override
    public ResponseResult<Object> deleteMenu(Long id) {
        LambdaQueryWrapper<Menu> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(Menu::getParentId, id);
        long count = count(queryWrapper);
        if (count > 0) {
            return ResponseResult.errorResult(AppHttpCodeEnum.HAS_CHILD_MENU);
        }
        removeById(id);
        return ResponseResult.success();
    }

    @Override
    public ResponseResult<List<SimpleMenuVo>> selectTreeAll() {
        List<Menu> list = list();
        List<SimpleMenuVo> simpleMenuVos = list.stream()
                .map(menu -> {
                    SimpleMenuVo simpleMenuVo = new SimpleMenuVo();
                    simpleMenuVo.setId(menu.getId());
                    simpleMenuVo.setLabel(menu.getMenuName());
                    simpleMenuVo.setParentId(menu.getParentId());
                    return simpleMenuVo;
                })
                .collect(Collectors.toList());
        List<SimpleMenuVo> menuTree = buildMenuTree(simpleMenuVos);
        return ResponseResult.success(menuTree);
    }



    private <T extends SimpleMenuVo> List<T> buildMenuTree(List<T> menuList) {
        return menuList.stream()
                //过滤出父菜单
                .filter(menuVo -> Objects.equals(menuVo.getParentId(), 0L))
                //把该结点作为父结点，查找该结点的子结点并赋值
                .peek(menuVo -> menuVo.setChildren(getChild(menuVo.getId(), menuList)))
                //返回结果
                .collect(Collectors.toList());
    }



    private List<Menu> builderMenuTree(List<Menu> menus, Long parentId) {
        List<Menu> menuTree = menus.stream()
                .filter(menu -> menu.getParentId().equals(parentId))
                .map(menu -> menu.setChildren(getChildren(menu, menus)))
                .collect(Collectors.toList());
        return menuTree;
    }

    /**
     * 获取存入参数的 子Menu集合
     * @param menu
     * @param menus
     * @return
     */
    private List<Menu> getChildren(Menu menu, List<Menu> menus) {
        List<Menu> childrenList = menus.stream()
                .filter(m -> m.getParentId().equals(menu.getId()))
                .map(m->m.setChildren(getChildren(m,menus)))
                .collect(Collectors.toList());
        return childrenList;
    }

    private <T extends SimpleMenuVo> List<T> getChild(Long id, List<T> menuList) {
        return menuList.stream()
                //过滤出该父节点的子节点
                .filter(menuVo -> Objects.equals(menuVo.getParentId(), id))
                //把该子结点作为父结点，递归查找出该子节点的的子结点并赋值
                .peek(childrenMenuVo -> childrenMenuVo.setChildren(getChild(childrenMenuVo.getId(), menuList)))
                .collect(Collectors.toList());
    }

    @Override
    public List<Long> selectMenuIdsByRoleId(Long id) {
        return baseMapper.selectMenuIdsByRoleId(id);
    }

    @Override
    public boolean removeByMap(Map<String, Object> columnMap) {
        return super.removeByMap(columnMap);
    }
}

