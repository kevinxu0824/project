package com.kevin.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.kevin.domain.ResponseResult;
import com.kevin.domain.dto.TagDto;
import com.kevin.domain.dto.TagListDto;
import com.kevin.domain.entity.Tag;
import com.kevin.domain.vo.PageVo;
import com.kevin.domain.vo.TagVo;
import com.kevin.mapper.TagMapper;
import com.kevin.service.TagService;
import com.kevin.utils.BeanCopyUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;

/**
 * 标签(Tag)表服务实现类
 *
 * @author makejava
 * @since 2023-03-07 10:56:05
 */
@Service("tagService")
public class TagServiceImpl extends ServiceImpl<TagMapper, Tag> implements TagService {


    @Override
    public ResponseResult<PageVo> getTagPageList(Integer pageNum, Integer pageSize, TagListDto tagListDto) {
        //分页查询
        LambdaQueryWrapper<Tag> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(StringUtils.hasText(tagListDto.getName()),Tag::getName,tagListDto.getName());
        queryWrapper.eq(StringUtils.hasText(tagListDto.getRemark()),Tag::getRemark,tagListDto.getRemark());

        Page<Tag> page = new Page<>();
        page.setCurrent(pageNum);
        page.setSize(pageSize);
        page(page, queryWrapper);
        //封装数据返回
        PageVo pageVo = new PageVo(page.getRecords(),page.getTotal());
        return ResponseResult.okResult(pageVo);
    }

    @Override
    public ResponseResult<TagVo> getVoById(Long id) {
        Tag tag = getById(id);
        TagVo tagVo = BeanCopyUtils.copyBean(tag, TagVo.class);
        return ResponseResult.success(tagVo);
    }

    @Override
    public ResponseResult<List<TagVo>> getAllTag() {
        LambdaQueryWrapper<Tag> queryWrapper = new LambdaQueryWrapper<>();
        // 规定返回的字段
        queryWrapper.select(Tag::getId, Tag::getName);
        List<Tag> list = list(queryWrapper);
        List<TagVo> tagVos = BeanCopyUtils.copyBeanList(list, TagVo.class);
        return ResponseResult.success(tagVos);
    }

    @Override
    public ResponseResult<Object> addTag(TagDto tagDto) {
        Tag tag = BeanCopyUtils.copyBean(tagDto, Tag.class);
        save(tag);
        return ResponseResult.success();
    }

    @Override
    public ResponseResult<Object> deleteTag(Long id) {
        removeById(id);
        return ResponseResult.success();
    }

    @Override
    public ResponseResult<Object> updateTag(TagDto tagDto) {
        updateById(BeanCopyUtils.copyBean(tagDto, Tag.class));
        return ResponseResult.success();
    }


}
