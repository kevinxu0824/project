package com.kevin.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.kevin.domain.ResponseResult;
import com.kevin.domain.dto.LinkListDto;
import com.kevin.domain.entity.Link;


/**
 * 友链(Link)表服务接口
 *
 * @author makejava
 * @since 2023-02-09 17:58:56
 */
public interface LinkService extends IService<Link> {
    ResponseResult getAllLink();

    ResponseResult addLink(Link link);

    ResponseResult getLinkById(Long id);

    ResponseResult updateLink(Link link);

    ResponseResult deleteLink(Long id);

    ResponseResult list(Integer pageNum, Integer pageSize, LinkListDto linkListDto);

    ResponseResult changeLinkStatus(Link link);
}
