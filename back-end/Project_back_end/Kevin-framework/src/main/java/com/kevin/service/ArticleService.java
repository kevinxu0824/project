package com.kevin.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.kevin.domain.ResponseResult;
import com.kevin.domain.dto.ArticleDto;
import com.kevin.domain.dto.PageDto;
import com.kevin.domain.dto.QueryArticleListDto;
import com.kevin.domain.entity.Article;
import com.kevin.domain.vo.ArticleListVo;
import com.kevin.domain.vo.PageVo;


public interface ArticleService extends IService<Article> {
    ResponseResult hotArticleList();

    ResponseResult articleList(Integer pageNum, Integer pageSize, Long categoryId);

    ResponseResult getArticleDetail(Long id);

    ResponseResult updateViewCount(Long id);

    ResponseResult<Object> addArticle(ArticleDto articleDto);

    ResponseResult<PageVo<ArticleListVo>> getAdminArticleList(QueryArticleListDto queryArticleListDto, PageDto pageDto);

    ResponseResult<ArticleDto> getAdminArticleDetail(Long id);

    ResponseResult<Object> updateArticle(ArticleDto articleDto);

    ResponseResult<Object> deleteArticle(Long id);

    boolean updateViewCountById(Long id,Long viewCount);
}
