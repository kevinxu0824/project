package com.kevin.controller;

import com.kevin.domain.ResponseResult;
import com.kevin.domain.dto.PageDto;
import com.kevin.domain.dto.RoleDto;
import com.kevin.domain.dto.RoleStatusDto;
import com.kevin.domain.vo.PageVo;
import com.kevin.domain.vo.RoleVo;
import com.kevin.domain.vo.SimpleRoleVo;
import com.kevin.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author :Yozuru
 * @since :2023/1/28 3:41
 */
@RestController
@RequestMapping("/system/role")
public class RoleController {
    @Autowired
    private RoleService roleService;
    @GetMapping("/list")
    @PreAuthorize("@ps.hasPermission('system:role:query')")
    public ResponseResult<PageVo<RoleVo>> listByPage(PageDto pageDto, String roleName, String status) {
        return roleService.getRoleListByPage(pageDto, roleName, status);
    }

    @PutMapping("/updateStatus")
    @PreAuthorize("@ps.hasPermission('system:role:edit')")
    public ResponseResult<Object> changeStatus(@RequestBody RoleStatusDto roleStatusDto) {
        return roleService.updateStatus(roleStatusDto.getRoleId(), roleStatusDto.getStatus());
    }

    @PostMapping
    @PreAuthorize("@ps.hasPermission('system:role:add')")
    public ResponseResult<Object> addRole(@RequestBody @Validated(RoleDto.Add.class)RoleDto roleDto) {
        return roleService.addRole(roleDto);
    }

    @GetMapping("/{id}")
    @PreAuthorize("@ps.hasPermission('system:role:edit')")
    public ResponseResult<RoleVo> getRoleById(@PathVariable Long id) {
        return roleService.getRoleById(id);
    }

    @PutMapping
    @PreAuthorize("@ps.hasPermission('system:role:edit')")
    public ResponseResult<Object> updateRole(@RequestBody @Validated(RoleDto.Update.class)RoleDto roleDto) {
        return roleService.updateRole(roleDto);
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("@ps.hasPermission('system:role:remove')")
    public ResponseResult<Object> deleteRole(@PathVariable Long id) {
        return roleService.deleteRoleById(id);
    }
    @GetMapping("/listAllRole")
    @PreAuthorize("@ps.hasPermission('system:user:add','system:user:edit','system:role:edit')")
    public ResponseResult<List<SimpleRoleVo>> listAllRole() {
        return roleService.getAllRole();
    }
}
