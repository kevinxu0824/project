package com.kevin.controller;

import com.kevin.domain.ResponseResult;
import com.kevin.domain.dto.CategoryDto;
import com.kevin.domain.dto.PageDto;
import com.kevin.domain.dto.QueryCategoryDto;
import com.kevin.domain.vo.CategoryListVo;
import com.kevin.domain.vo.CategoryVo;
import com.kevin.domain.vo.PageVo;
import com.kevin.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/content/category")
@PreAuthorize("@ps.hasPermission('content:category:list')")
public class CategoryController {

    @Autowired
    private CategoryService categoryService;

    @GetMapping("/listAllCategory")
    @PreAuthorize("@ps.hasPermission('content:article:writer')")
    public ResponseResult<List<CategoryVo>> listAllCategory() {
        return categoryService.getAllCategory();
    }

    @GetMapping("/list")
    public ResponseResult<PageVo<CategoryListVo>> listCategory(PageDto pageDto, QueryCategoryDto queryCategoryDto) {
        return categoryService.listCategoryByPage(pageDto, queryCategoryDto);
    }

    @PostMapping
    public ResponseResult<Object> addCategory(@RequestBody @Validated({CategoryDto.Add.class}) CategoryDto categoryDto) {
        return categoryService.addCategory(categoryDto);
    }

    @GetMapping("/{id}")
    public ResponseResult<CategoryDto> getCategory(@PathVariable("id") Integer id) {
        return categoryService.getCategoryById(id);
    }

    @PutMapping
    public ResponseResult<Object> updateCategory(@RequestBody @Validated({CategoryDto.Update.class})CategoryDto categoryDto) {
        return categoryService.updateCategory(categoryDto);
    }
    @DeleteMapping("/{id}")
    public ResponseResult<Object> deleteCategory(@PathVariable("id") Long id) {
        return categoryService.deleteCategory(id);

}
}