package com.kevin.controller;

import com.kevin.config.SecurityConfig;
import com.kevin.domain.ResponseResult;
import com.kevin.domain.entity.LoginUser;
import com.kevin.domain.entity.Menu;
import com.kevin.domain.entity.User;
import com.kevin.domain.vo.AdminUserInfoVo;
import com.kevin.domain.vo.RoutersVo;
import com.kevin.domain.vo.UserInfoVo;
import com.kevin.enums.AppHttpCodeEnum;
import com.kevin.handler.exception.SystemException;
import com.kevin.service.LoginService;
import com.kevin.service.MenuService;
import com.kevin.service.RoleService;
import com.kevin.utils.BeanCopyUtils;
import com.kevin.utils.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class LoginController {
    @Autowired
    private LoginService loginService;

    @Autowired
    private MenuService menuService;

    @Autowired
    private RoleService roleService;

    @PostMapping("/user/login")
    public ResponseResult login(@RequestBody User user){
        if(!StringUtils.hasText(user.getUserName())){
            //提示 必须要传用户名
            throw new SystemException(AppHttpCodeEnum.REQUIRE_USERNAME);
        }
        return loginService.login(user);
    }

    @GetMapping("getInfo")
    public ResponseResult<AdminUserInfoVo> getInfo(){

        LoginUser loginUser= SecurityUtils.getLoginUser();

        List<String> perms = menuService.selectPermsByUserId(loginUser.getUser().getId());

        List<String> roleKeyList = roleService.selectRoleKeyByUserId(loginUser.getUser().getId());

        //获取用户信息
        User user = loginUser.getUser();
        UserInfoVo userInfoVo = BeanCopyUtils.copyBean(user, UserInfoVo.class);
        //封装数据返回

        AdminUserInfoVo adminUserInfoVo = new AdminUserInfoVo(perms,roleKeyList,userInfoVo);
        return ResponseResult.okResult(adminUserInfoVo);
    }

    @GetMapping("getRouters")
    public ResponseResult<RoutersVo> getRouters(){
        Long userId = SecurityUtils.getUserId();
        //查询menu 结果是tree的形式
        List<Menu> menus = menuService.selectRouterMenuTreeByUserId(userId);
        //封装数据返回
        return ResponseResult.okResult(new RoutersVo(menus));
    }
    @PostMapping("/user/logout")
    public ResponseResult logout(){
        return loginService.logout();
    }

}