package com.kevin.controller;


import com.kevin.domain.ResponseResult;
import com.kevin.domain.dto.TagDto;
import com.kevin.domain.dto.TagListDto;
import com.kevin.domain.vo.PageVo;
import com.kevin.domain.vo.TagVo;
import com.kevin.service.TagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author Yozuru
 */

@RestController
@RequestMapping("/content/tag")
@PreAuthorize("@ps.hasPermission('content:tag:index')")
public class TagController {
    @Autowired
    private TagService tagService;
//    @GetMapping("/list")
//    public ResponseResult<PageVo> list(Integer pageNum, Integer pageSize, TagListDto tagListDto){
//        return tagService.pageTagList(pageNum,pageSize,tagListDto);
//    }

    @GetMapping("/list")
    public ResponseResult<PageVo> getPageList(Integer pageNum, Integer pageSize, TagListDto tagListDto) {
        return tagService.getTagPageList(pageNum, pageSize, tagListDto);
    }

    @GetMapping("/{id}")
    public ResponseResult<TagVo> getTagById(@PathVariable Long id) {
        return tagService.getVoById(id);
    }

    @GetMapping("/listAllTag")
    @PreAuthorize("@ps.hasPermission('content:article:writer')")
    public ResponseResult<List<TagVo>> listAllTag() {
        return tagService.getAllTag();
    }


    @PostMapping
    public ResponseResult<Object> addTag(@RequestBody @Validated(TagDto.Add.class) TagDto tagDto) {
        return tagService.addTag(tagDto);
    }

    @DeleteMapping("/{id}")
    public ResponseResult<Object> deleteTag(@PathVariable("id") Long id) {
        return tagService.deleteTag(id);
    }

    @PutMapping
    public ResponseResult<Object> update(@RequestBody @Validated(TagDto.Update.class) TagDto tagDto) {
        return tagService.updateTag(tagDto);
    }

}
