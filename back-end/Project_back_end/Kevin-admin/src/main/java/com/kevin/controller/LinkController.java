package com.kevin.controller;

import com.kevin.domain.ResponseResult;
import com.kevin.domain.dto.LinkListDto;
import com.kevin.domain.entity.Link;
import com.kevin.service.LinkService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/content/link")
public class LinkController {

    @Autowired
    private LinkService linkService;


    @GetMapping("/getAllLink")
    public ResponseResult getAllLink(){
        return linkService.getAllLink();
    }

    /**
     * 分页查询
     * @param pageNum
     * @param pageSize
     * @param linkListDto
     * @return
     */
    @GetMapping("/list")
    public ResponseResult list(@RequestParam("pageNum") Integer pageNum,
                               @RequestParam("pageSize") Integer pageSize,
                               LinkListDto linkListDto){
        return linkService.list(pageNum,pageSize,linkListDto);
    }

    /**
     * 新增友链
     * @param link
     * @return
     */
    @PostMapping()
    public ResponseResult addLink(@RequestBody Link link){
        return linkService.addLink(link);
    }


    /**
     * 根据id查询友链
     * @param id
     * @return
     */
    @GetMapping("/{id}")
    public ResponseResult getLinkById(@PathVariable Long id){
        return linkService.getLinkById(id);
    }

    /**
     * 修改友链
     * @param link
     * @return
     */
    @PutMapping
    public ResponseResult updateLink(@RequestBody Link link){
        return linkService.updateLink(link);
    }

    /**
     * 删除友链
     * @param id
     * @return
     */
    @DeleteMapping("/{id}")
    public ResponseResult deleteLink(@PathVariable Long id){
        return linkService.deleteLink(id);
    }

    /**
     * 改变友链状态
     * @param link
     * @return
     */
    @PutMapping("/changeLinkStatus")
    public ResponseResult changeLinkStatus(@RequestBody Link link){
        return linkService.changeLinkStatus(link);
    }


}
