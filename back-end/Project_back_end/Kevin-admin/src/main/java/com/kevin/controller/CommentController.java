package com.kevin.controller;


import com.kevin.domain.ResponseResult;
import com.kevin.domain.dto.PageDto;
import com.kevin.domain.dto.QueryCommentListDto;
import com.kevin.domain.vo.CommentListVo;
import com.kevin.domain.vo.PageVo;
import com.kevin.service.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;

@RestController
@RequestMapping("/content/comment")
public class CommentController {
    @Autowired
    private CommentService commentService;

    @GetMapping("/list")
    public ResponseResult<PageVo<CommentListVo>> listCategory(PageDto pageDto, QueryCommentListDto queryCommentListDto) {
        return commentService.listCommentByPage(pageDto, queryCommentListDto);
    }

    @DeleteMapping("/{ids}")
    @PreAuthorize("@ps.hasPermission('content:comment:remove')")
    public ResponseResult<Object> deleteUser(@PathVariable Long[] ids){
        return commentService.deleteWithValidById(Arrays.asList(ids),true);
    }

}
